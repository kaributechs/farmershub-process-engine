package com.kaributechs.farmershubprocessengine;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FarmershubProcessEngineApplication {

    public static void main(String[] args) {
        SpringApplication.run(FarmershubProcessEngineApplication.class, args);
    }

}
